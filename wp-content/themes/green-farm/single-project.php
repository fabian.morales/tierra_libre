<?php
/**
 * @package 	WordPress
 * @subpackage 	Green Farm
 * @version		1.0.0
 * 
 * Single Project Template
 * Created by CMSMasters
 * 
 */


get_header();


$cmsmasters_option = green_farm_get_global_options();


$project_tags = get_the_terms(get_the_ID(), 'pj-tags');


$cmsmasters_project_sharing_box = get_post_meta(get_the_ID(), 'cmsmasters_project_sharing_box', true);

$cmsmasters_project_author_box = get_post_meta(get_the_ID(), 'cmsmasters_project_author_box', true);

$cmsmasters_project_more_posts = get_post_meta(get_the_ID(), 'cmsmasters_project_more_posts', true);


echo '<!--_________________________ Start Content _________________________ -->' . "\n" . 
'<div class="middle_content entry">';


if (have_posts()) : the_post();
	echo '<div class="portfolio opened-article">' . "\n";
	
	
	get_template_part('theme-framework/postType/portfolio/project-single');
	
	
	if ($cmsmasters_project_sharing_box == 'true') {
		green_farm_sharing_box(esc_html__('Like this project?', 'green-farm'));
	}
	
	
	if ($cmsmasters_option['green-farm' . '_portfolio_project_nav_box']) {
		green_farm_prev_next_posts();
	}
	
	
	if ($cmsmasters_project_author_box == 'true') {
		green_farm_author_box(esc_html__('About author', 'green-farm'), 'h3', 'h5');
	}
	
	
	if ($project_tags) {
		$tgsarray = array();
		
		
		foreach ($project_tags as $tagone) {
			$tgsarray[] = $tagone->term_id;
		}  
	} else {
		$tgsarray = '';
	}
	
	
	if ($cmsmasters_project_more_posts != 'hide') {
		green_farm_related( 
			'h3', 
			esc_html__('More projects', 'green-farm'), 
			esc_html__('No projects found', 'green-farm'), 
			$cmsmasters_project_more_posts, 
			$tgsarray, 
			$cmsmasters_option['green-farm' . '_portfolio_more_projects_count'], 
			$cmsmasters_option['green-farm' . '_portfolio_more_projects_pause'], 
			'project' 
		);
	}
	
	
	comments_template(); 
	
	
	echo '</div>';
endif;


echo '</div>' . "\n" . 
'<!-- _________________________ Finish Content _________________________ -->' . "\n\n";


get_footer();

