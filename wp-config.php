<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tierra_libre');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'desarrollo');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'wvytibu78nw106zzouvchk9vf5kzatliq0clwwlk6bnq3unth5hufz4tavcbcyn3');
define('SECURE_AUTH_KEY',  'r2b8yfocqa4gwfnxeu8qf9u9rzjqxjsbwifpadofvvsqaqge5nxrd7dryqlnnvx3');
define('LOGGED_IN_KEY',    'nve0i0hbvlaxskqqvjlfjfm7jbckb0p4njytweibti8feqlbfenb0lqscbvogz40');
define('NONCE_KEY',        'zsouxnugvk4okeyzbfwn19om14uvvyszpin1qifnwgketb4xsku77khk9izw6rxq');
define('AUTH_SALT',        '3mcdbqnbdob0jnygihaun9jnipoeyqvlh9wu07457lzilrx0sz7q9euyfe2hawm4');
define('SECURE_AUTH_SALT', 'itr6esbcsimu1dw5xt15t2qolvnfyerfoaxpcji7mtz9r3my1wigopsudygjibam');
define('LOGGED_IN_SALT',   'hxyzf8qmwwhordlbq1gyq1lwuudiqxhg7bcgwxz5tgeab3evhggzcroto78vqy12');
define('NONCE_SALT',       'pmdj1rsiqumwslshgo4u9poyz5by6jvyqwi5run56xwtisnslxgfihqy6dctaose');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpxn_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
